#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QIcon>
#include <QPixmap>
#include <QPalette>

#include "linklist.h"
#include "clinklist.h"
#include "dlinklist.h"

//主页面

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton1_clicked(); //单链表按钮

    void on_pushButton2_clicked(); //循环链表按钮

    void on_pushButton3_clicked(); //双向链表按钮

private:
    Ui::MainWindow *ui;
    LinkList linkList;
    CLinkList clinklist;
    DLinkList dlinklist;

    void initUI();
};

#endif // MAINWINDOW_H
