﻿#ifndef LINKLIST_H
#define LINKLIST_H

#include <QWidget>
#include <QFile>
#include <QTextBrowser>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QIntValidator>

#include "uidefine.h"
#include "lnode.h"

namespace Ui {
class LinkList;
}

class LinkList : public QWidget
{
    Q_OBJECT
    /*

    信号和槽是Qt应用开发的基础，它可是将两个毫无关系的对象连接在一起，槽和普通的C++函数是一样的，
    只是当它和信号连接在一起后，当发送信号的时候，槽会自动被调用只有加入了Q_OBJECT，你才能使用QT中的signal和slot机制。

    所有QObject的派生类在官方文档中都推荐在头文件中放置宏Q_OBJECT，那么该宏到底为我们做了哪些工作？在qobjectdef.h中有下面的代码
    https://blog.csdn.net/weixin_39609623/article/details/82966753
      */

public:
    explicit LinkList(QWidget *parent = 0);			//构造函数
    ~LinkList();									//析构函数

private slots:
    void on_pushButtonInit_clicked();					//槽函数：点击创建

    void on_pushButtonClear_clicked();                  //槽函数：点击清空

    void on_pushButtonInsert_clicked();					//槽函数：点击插入

    void on_pushButtonRandomInsert5_clicked();			//随机插入五个节点到链表末尾

    void on_pushButtonDelete_clicked();					//槽函数：点击删除

    void on_pushButtonLocate_clicked();					//槽函数：点击查找

    void on_horizontalSlider_valueChanged(int value);	//调整演示的速度快慢

protected:
    Ui::LinkList *ui;               //UI指针
    QGraphicsScene *scene;          //视图场景指针
    QGraphicsTextItem *headLabel;   //链表符号指针
    MyArrowItem *headArrow;         //符号箭头指针
    LNode *head;                    //头结点指针
    int countNode;                  //节点个数计数（不包括头结点）
    int sleepTime;                  //演示延时时间（ms）

    void initTextBrowser();			//初始设置文本显示区
    void initUI();					//初始UI控件
    void initSceneView();			//初始化视图框架
    void adjustController();		//操作之后调整右侧输入和显示控件

    QPoint getLNodePos(int nodeNumber);						//计算节点的Scene坐标
    void addLNodeGraphicsItem(LNode *pl, QPoint coord);		//添加节点的GraphicsItem
    void adjustLNodeArrow(LNode *pLNode, int nodeNumber);	//调整节点的箭头
    void adjustLNodePos(LNode *pLNode, QPoint coord);		//调整节点Scene坐标
    void setLinkListNormalBrush();							//设置链表所有节点显示模式

    void initLinkList();						//创建链表初始化
    void insertLNode(int pos, QString elem);	//插入节点到链表
    void deleteLNode(int pos, QString &elem);	//删除链表节点
    bool locateLNode(int &pos,QString elem);	//查找链表节点
    void destroySelf();							//释放申请的内存空间

private:
    //静态常数据，类似于宏
    const static QBrush normalBursh, visitedBrush, markBrush;//三种颜色
    const static QFont headLabelFont, dataFont; //头指针会显示“头结点”
    const static QIntValidator dataValidator; //这里的链表只能输入Int validator是检测器的意思
};

#endif // LINKLIST_H
