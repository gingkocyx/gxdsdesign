#ifndef LNODE_H
#define LNODE_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QBrush>

#include "myarrowitem.h"

class LinkList;
class CLinkList;

class LNode
{
public:
    LNode(QString dt, LNode *nt);       //构造函数
    ~LNode();                           //析构函数
    //rect矩形  现在猜测意思应该是 指针域的rectangle 、 数据域的rectangle
    void setValueRect(QGraphicsRectItem* vRect);
    void setPointerRect(QGraphicsRectItem* pRect);
    void setTextRect(QGraphicsTextItem* vText);
    void setArrowVector(std::vector<MyArrowItem*> aVector);
    //brush 刷子刷颜色的 灰色 绿色 黄色
    void setNodeStatus(QBrush brush);
    void removeAll(QGraphicsScene *scene);

protected:
    QString data;       //节点数据域
    LNode * next;       //节点指针域

    //节点图形项
    QGraphicsRectItem * valueRect, *pointerRect;
    QGraphicsTextItem * valueText, *pointerText;
    std::vector<MyArrowItem*> arrowVector;

public:
    friend class LinkList;
    friend class CLinkList;
    /*
       Friend Classes(友元类) C++中的friend关键字
        其实做这样的事情:在一个类中指明其他的类(或者)函数能够
        直接访问该类中的private和protected成员
    */
};

#endif // LNODE_H
