#include "lnode.h"
#include <QDebug>
LNode::LNode(QString dt, LNode *nt)
{
    data=dt;
    next=nt;
    pointerRect=valueRect=NULL;
    valueText=pointerText=NULL;
}

LNode::~LNode()
{

}

void LNode::setValueRect(QGraphicsRectItem* vRect)
{
    valueRect=vRect;
//    printf("setValueRect");
    qDebug()<<"setValueRect"<<"\n";
}

void LNode::setPointerRect(QGraphicsRectItem* pRect)
{
    pointerRect=pRect;
    qDebug()<<"setPointerRect"<<"\n";
}

void LNode::setTextRect(QGraphicsTextItem* vText)
{
    valueText=vText;
    qDebug()<<"setTextRect"<<"\n";
}

void LNode::setArrowVector(std::vector<MyArrowItem*> aVector)
{
    arrowVector=aVector;
    qDebug()<<"setArrowVector"<<"\n";
}

void LNode::setNodeStatus(QBrush brush)
{
    valueRect->setBrush(brush);
    qDebug()<<"setNodeStatus"<<"\n";
}

void LNode::removeAll(QGraphicsScene *scene)
{
    //removeAll是要移除
    //valueRect 这些都是指针
    //如果不为空，就delete
    if(valueRect)
    {
        scene->removeItem(valueRect);
        delete valueRect;
        valueRect=NULL;
    }

    if(pointerRect)
    {
        scene->removeItem(pointerRect);
        delete pointerRect;
        pointerRect=NULL;
    }

    if(valueText)
    {
        scene->removeItem(valueText);
        delete valueText;
        valueText=NULL;
    }

    if(pointerText)
    {
        scene->removeItem(pointerText);
        delete pointerText;
        pointerText=NULL;
    }

    for(auto &a:arrowVector)
        scene->removeItem(a);
    arrowVector.clear();
}
