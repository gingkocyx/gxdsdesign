#include<QDebug>
#include <math.h>
#include "linklist.h"
#include "ui_linklist.h"
#include "myarrowitem.h"

void sleep(unsigned int msec);

//这是给头文件里面的private属性赋值
const QBrush LinkList::normalBursh=QBrush(Qt::GlobalColor::darkGray); //具体赋值
const QBrush LinkList::visitedBrush=QBrush(Qt::GlobalColor::yellow);
const QBrush LinkList::markBrush=QBrush(Qt::GlobalColor::green);

const QFont LinkList::headLabelFont=QFont("Consolas");
const QFont LinkList::dataFont=QFont("Consolas",8);

const QIntValidator LinkList::dataValidator(-999999999,999999999);

//构造函数
//冒号后面是父类的构造函数 https://blog.csdn.net/lusirking/article/details/83988421
//https://blog.csdn.net/huhuandk/article/details/103206310

LinkList::LinkList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LinkList)
{
    //初始化数据

    ui->setupUi(this);
    //初始设置文本显示区
    //HTML文件那个文本
    initTextBrowser();
    //初始UI控件
    //未创建时，除创建按钮外，其余按键一律无效
    initUI();

    countNode=0;//结点数
    head=NULL;
    headLabel=NULL;
    headArrow=NULL;
    scene=NULL; //视图场景指针 (暂时理解为画布）
    sleepTime=MAX_SLEEP_TIME>>1; //演示最大延时MAX_SLEEP_TIME 一开始有右移1除以2 默认在中间500ms ,然后后面可以对MAX进行运算调整延时

    ui->horizontalSlider->setValue(MAX_SLIDER>>1); //条子 放在中间
    //#define MAX_SLIDER 20            //速度调节器的最大倍数值
    srand(time(NULL)); //随机生成五个节点

    this->setWindowTitle("单链表");
}

//析构函数
//关闭窗口调用
LinkList::~LinkList()
{

    destroySelf();//清空的时候调用
    delete ui;
    qDebug()<<"调用析构函数";
}

//初始设置文本显示区
void LinkList::initTextBrowser(){
    //加载html文件
    QFile file(":/html/html/LinkList.html");
    file.open(QIODevice::ReadOnly); //只读形式打开
    QString htmlString=file.readAll();  //读取html中的内容写入字符串
    ui->textBrowser->setHtml(htmlString); //
    ui->textBrowser->setOpenLinks(true); //点击之后有无效果

    ui->textBrowser->setOpenExternalLinks(true);//允许访问网址链接

    file.close();
}

//初始UI控件
void LinkList::initUI()
{
    //set background image
    QPixmap background(":/ico/resource/background.png"); //背景图片 (操作界面的背景图片）
    QPalette palette; //palette意思:调色板
    palette.setBrush(QPalette::Background,background);
    this->setPalette(palette);

    //未创建时，除创建按钮外，其余按键一律无效
    ui->pushButtonClear->setEnabled(false);
    ui->pushButtonInsert->setEnabled(false);
    ui->pushButtonRandomInsert5->setEnabled(false);
    ui->comboBoxInsert->setEnabled(false); //插入链表的位置的下拉框
    ui->lineEditInsert->setEnabled(false); //插入值的输入框

    //同理
    ui->pushButtonDelete->setEnabled(false);
    ui->comboBoxDelete->setEnabled(false);
    ui->lineEditDelete->setEnabled(false);

    ui->pushButtonLocate->setEnabled(false);
    ui->comboBoxLocate->setEnabled(false);
    ui->lineEditLocate->setEnabled(false);

    //对应界面上的状态提示
    ui->lineEditState->setEnabled(false);

    //设置字体
    ui->lineEditInsert->setFont(dataFont);
    ui->lineEditDelete->setFont(dataFont);
    ui->lineEditLocate->setFont(dataFont);
    ui->lineEditState->setFont(dataFont);
    //输入框里面默认的文本
    ui->lineEditInsert->setPlaceholderText("插入值：Int");
    ui->lineEditLocate->setPlaceholderText("查找值：Int");
    ui->lineEditInsert->setValidator(&LinkList::dataValidator);//把那个Int检测器 set过来
    ui->lineEditLocate->setValidator(&LinkList::dataValidator);
    ui->lineEditState->setText("请选择操作");

    //速度调节的slider设置最大最小
    ui->horizontalSlider->setMinimum(0);
    ui->horizontalSlider->setMaximum(MAX_SLIDER);
    ui->horizontalSlider->setTickPosition(QSlider::TicksBelow);//在水平滑块下方绘制刻度线

}


//初始化视图框架 //是在创建链表初始化initLinkList里面调用的
void LinkList::initSceneView()
{
    //为 view 配置相应 scene
    scene=new QGraphicsScene;
    scene->setSceneRect(0,0,SCENE_MAX_W,SCENE_MAX_H);
    ui->graphicsView->setScene(scene);

    //添加链表符号及指针箭头
    headLabel=scene->addText("L",headLabelFont);
    headLabel->setPos((VALUE_RECT_W-RECT_H)>>1,SPACING);
sleep(sleepTime);
    headArrow=new MyArrowItem(ARROW_LEN); //箭头
    scene->addItem(headArrow);
    headArrow->setPos(ARROW_H_OFFSET,ARROW_V_OFFSET); //箭头的POS H 水平  V 垂直
}

//操作之后调整右侧输入和显示控件
void LinkList::adjustController()
{
    //当经过插入或删除操作后，节点个数可能会改变，需考虑一些操作的合法性
    //比如还没创建的时候就不能清空
    ui->pushButtonDelete->setEnabled(countNode);
    ui->comboBoxDelete->setEnabled(countNode);

    //ui和后面的数据要同步
    if(ui->comboBoxDelete->count()!=countNode)
    {
        QStringList qStringList;
        for(int i=1;i<=countNode;++i)
            qStringList.push_back(QString::number(i));

        ui->comboBoxInsert->clear();
        ui->comboBoxInsert->addItems(qStringList);

        ui->comboBoxDelete->clear();
        ui->comboBoxDelete->addItems(qStringList);
    }
    //countNode是节点个数计数（不包括头结点）,但是UI上面有头结点 所以要+1
    if(ui->comboBoxInsert->count()!=countNode+1)
        ui->comboBoxInsert->addItem(QString::number(countNode+1));

    ui->lineEditDelete->setText(" ");
    ui->comboBoxLocate->clear();
}

//计算节点的Scene坐标
QPoint LinkList::getLNodePos(int nodeNumber)
{
    //一行的节点数量rowN
    const static int rowN=SCENE_MAX_W/NODE_W;
    //返回点的坐标
//#define NODE_W (VALUE_RECT_W+POINTER_RECT_W+25)             //每个节点占用宽度
//#define NODE_H (RECT_H+SPACING+SPACING)                     //每个节点占用高度
    return QPoint((nodeNumber+1)%rowN*NODE_W,(nodeNumber+1)/rowN*NODE_H);
}

//添加节点的GraphicsItem
//https://www.cnblogs.com/aiguona/p/10002255.html
void LinkList::addLNodeGraphicsItem(LNode *pl, QPoint coord)
{
    int x=coord.x(), y=coord.y();
qDebug()<<"x="<<x<<" y="<<y<<"\n";
        //根据这些Rect的不同特点,选择需要的参数
    //QPen 绘制轮廓线
    pl->valueRect   =scene->addRect(x,y+SPACING,VALUE_RECT_W,RECT_H,QPen(),LinkList::markBrush); //markBrush绿色
    pl->pointerRect =scene->addRect(x+VALUE_RECT_W,y+SPACING,POINTER_RECT_W,RECT_H);
    //const QFont LinkList::dataFont=QFont("Consolas",8);
    pl->valueText   =scene->addText(pl->data,LinkList::dataFont); //数据域
    pl->valueText->setPos(x,y+SPACING+5);
    if(pl->next==NULL)
    {
        pl->pointerText=scene->addText(" ^",LinkList::dataFont); //指针域
        pl->pointerText->setPos(x+VALUE_RECT_W, y+SPACING+5);
    }
}

//调整节点的箭头 //这个函数是在删除、插入节点的时候调用
void LinkList::adjustLNodeArrow(LNode *pLNode, int nodeNumber)
{   //for(auto &a:b)中加了引用符号，可以对容器中的内容进行赋值，即可通过对a赋值来做到容器b的内容填充。

    //然后如果插入的时候，也remove可能是先擦后画的原理
    for(auto &a:pLNode->arrowVector)
        scene->removeItem(a);
    if(pLNode->next==NULL)
        return ;
    //屏幕上的坐标
    //调用上面的getLNodePos的函数
    QPoint myCoord=getLNodePos(nodeNumber);
    //计算下一个点
    QPoint nextCoord=getLNodePos(nodeNumber+1);
    //通过两个点的坐标和矩形的长宽来绘制箭头的位置
    MyArrowItem *pArrow;

    //不用换行的箭头的绘制
    if(myCoord.y()==nextCoord.y())
    {
        //节点不是一行最后节点
        pArrow=new MyArrowItem(ARROW_LEN);
        scene->addItem(pArrow);//加上箭头
        pArrow->setPos(myCoord.x()+ARROW_H_OFFSET,myCoord.y()+ARROW_V_OFFSET);
        pLNode->arrowVector.push_back(pArrow);
    }
    //换行的箭头的绘制
    else
    {
        //节点是一行中最后节点
        //换行的时候需要多个点来确定箭头，才方便绘制
        QPoint point1(myCoord.x()+ARROW_H_OFFSET,myCoord.y()+ARROW_V_OFFSET);
        QPoint point2(point1.x(),point1.y()+NODE_H/2);
        QPoint point3(VALUE_RECT_W>>1,point2.y());

        pArrow=new MyArrowItem(NODE_H>>1,2,0);
        scene->addItem(pArrow);
        pArrow->setPos(point1);
        pLNode->arrowVector.push_back(pArrow);

        pArrow=new MyArrowItem(point2.x()-point3.x(),3,0);
        scene->addItem(pArrow);
        pArrow->setPos(point2);
        pLNode->arrowVector.push_back(pArrow);

        pArrow=new MyArrowItem(SPACING,2);
        scene->addItem(pArrow);
        pArrow->setPos(point3);
        pLNode->arrowVector.push_back(pArrow);
    }
}

//调整节点的Scene坐标
void LinkList::adjustLNodePos(LNode *pLNode, QPoint coord)
{
    int x=coord.x(), y=coord.y();
    pLNode->valueRect->setRect(x,y+SPACING,VALUE_RECT_W,RECT_H);
    pLNode->pointerRect->setRect(x+VALUE_RECT_W,y+SPACING,POINTER_RECT_W,RECT_H);
    pLNode->valueText->setPos(x,y+SPACING+5);
    if(pLNode->pointerText)
        pLNode->pointerText->setPos(x+VALUE_RECT_W, y+SPACING+5);
}

//设置链表所有节点显示模式
void LinkList::setLinkListNormalBrush()
{
    for(LNode *pLNode=head;pLNode;pLNode=pLNode->next)
        pLNode->setNodeStatus(LinkList::normalBursh);
}

//创建链表初始化
void LinkList::initLinkList()
{
    initSceneView();
sleep(sleepTime);
    head=new LNode("头结点", NULL);
    addLNodeGraphicsItem(head, getLNodePos(0));
}

//插入节点到链表
//pos是插入的位置 在界面里面有
void LinkList::insertLNode(int pos, QString elem)
{
    //pInsertNode 新建要插入的节点
    LNode *pInsertNode=NULL;
    LNode *pLNode=head;
    head->setNodeStatus(LinkList::visitedBrush); //更新颜色
    //找到前驱节点指针
    for(int i=0;i<pos-1;++i)
    {
        sleep(sleepTime);

        pLNode=pLNode->next;
        pLNode->setNodeStatus(LinkList::visitedBrush);
    }
sleep(sleepTime);
    if(pLNode->next==NULL){
        scene->removeItem(pLNode->pointerText);
        pLNode->pointerText=NULL;
    }

    //新节点插入到链表中
    //elem是data  QString data;       //节点数据域
    pInsertNode=new LNode(elem,pLNode->next);
    pLNode->next=pInsertNode;
    ++countNode;

    //添加图形Item
    //前面是一个参数是结点 ， 后面一个getLNodePos返回的是QPoint
    addLNodeGraphicsItem(pInsertNode,getLNodePos(pos));
    //插入时对每个节点的坐标和每个箭头的坐标位置进行调整
    //是调整的插入的节点的后面的节点的位置
    for(--pos;pos<=countNode;++pos)
    {
        sleep(sleepTime);

        adjustLNodePos(pLNode,getLNodePos(pos));    //调整节点坐标位置
        adjustLNodeArrow(pLNode,pos);               //调整每个节点箭头
        pLNode=pLNode->next;
    }
}

//删除链表节点
void LinkList::deleteLNode(int pos, QString &elem)
{
    LNode *pDeleteNode=NULL;
    LNode *pLNode=head;

    head->setNodeStatus(LinkList::visitedBrush);

    //找到前驱节点指针
    for(int i=0;i<pos-1;++i)
    {
        sleep(sleepTime);

        pLNode=pLNode->next;
        pLNode->setNodeStatus(LinkList::visitedBrush);
    }
    //前面的步骤和插入时一样
sleep(sleepTime);
    pDeleteNode=pLNode->next;
    pLNode->next=pDeleteNode->next;
    elem=pDeleteNode->data; //保存删除的数据域 会在界面上显示

    //删除节点,移除图形Item
    pDeleteNode->removeAll(scene);
    delete pDeleteNode;
    --countNode;
    //针对只剩下头结点的情况
    if(pLNode->next==NULL){
        QPoint coord=getLNodePos(pos-1);
        pLNode->pointerText=scene->addText(" ^",LinkList::dataFont);
        pLNode->pointerText->setPos(coord.x()+VALUE_RECT_W, coord.y()+SPACING+5);
    }

    //把删除的节点的后面的节点的坐标往前调整
    for(--pos;pos<=countNode;++pos)
    {
        sleep(sleepTime);

        adjustLNodePos(pLNode,getLNodePos(pos));    //调整节点坐标位置
        adjustLNodeArrow(pLNode,pos);               //调整节点箭头
        pLNode=pLNode->next;
    }
}

//查找链表节点
bool LinkList::locateLNode(int &pos, QString elem)
{
    LNode *pLNode=head; //单链表得从头结点开始查

    head->setNodeStatus(LinkList::visitedBrush); //黄色 头结点肯定是会变成黄色

    //pLNode不为空
    //找到了就不继续往后找了
    //pLNode==NULL对应的情况应该是只有头结点
    //pLNode->next==NULL说明找到最后一个还没有找到
    for(pos=1;pLNode&&pLNode->next&&pLNode->next->data!=elem;++pos)
    {
        sleep(sleepTime);

        pLNode=pLNode->next;
        pLNode->setNodeStatus(LinkList::visitedBrush); //黄色
    }
sleep(sleepTime);
    //找到相应节点
    //那两种为NULL的情况都不存在
    if(pLNode&&pLNode->next){
        pLNode->next->setNodeStatus(LinkList::markBrush); //标出绿色
        return true;
    }
    return false;
}

//释放申请的内存空间
void LinkList::destroySelf()
{
    if(scene==NULL) //2D容器 场景指针
        return ;

    LNode *pLNode=head, *qLNode;
    //从头结点的下一个结点开始释放
    //退出循环的条件 pLNode是NULL的时候（即释放完成了）
    for(;pLNode;pLNode=qLNode)
    {
        sleep(sleepTime);
        qLNode=pLNode->next;
        pLNode->removeAll(scene);       //移除每个节点的图形Item
        delete pLNode;      //释放内存
    }
sleep(sleepTime+1000);  //演示延时时间（ms）
    scene->removeItem(headLabel);   //移除链表符号Item
    scene->removeItem(headArrow);   //移除链表符号后的箭头Item
    delete headLabel;       //释放内存
    delete headArrow;       //释放内存
    scene=NULL;

    countNode=0;
    qDebug()<<"调用destorySelf";

}

//槽函数：点击创建
void LinkList::on_pushButtonInit_clicked()
{
    //若已经建立，需要清除重建
    //destroySelf有个判断
    //第一次创建的时候调用destroySelf会直接return
//    if(scene==NULL) //2D容器 场景指针
//        return ;
    destroySelf();

    initLinkList();
    qDebug()<<"on_pushButtonInit_clicked 槽函数:点击创建"<<"\n";
    //没创建之前其他那些按钮都是灰色的，然后都可以用了
    ui->pushButtonClear->setEnabled(true);
    ui->pushButtonInsert->setEnabled(true);
    ui->pushButtonRandomInsert5->setEnabled(true);
    ui->comboBoxInsert->setEnabled(true);
    ui->lineEditInsert->setEnabled(true);
    ui->pushButtonLocate->setEnabled(true);
    ui->lineEditLocate->setEnabled(true);
    //调整显示控件
    adjustController();
    //状态提示栏变为绿色并显示Success
    ui->lineEditState->setPalette(Qt::GlobalColor::green);
    ui->lineEditState->setText("Create Success!");
}

//槽函数：点击清空
void LinkList::on_pushButtonClear_clicked()
{
    //把原先有的节点之类的先去掉
    destroySelf();
    //清空之后会回到初始的UI
    initUI();
    qDebug()<<"on_pushButtonClear_clicked 槽函数:点击清空"<<"\n";
}

//槽函数：点击插入
void LinkList::on_pushButtonInsert_clicked()
{
    qDebug()<<"on_pushButtonInsert_clicked 槽函数:点击插入"<<"\n";
    setLinkListNormalBrush(); //灰色的brush
sleep(sleepTime);
    //要插入的值
    QString edit=ui->lineEditInsert->text();

    //若输入无效或未输入
    if(edit.isEmpty())
    {
        ui->lineEditState->setPalette(Qt::GlobalColor::red);
        ui->lineEditState->setText("Please Input!");
        return ;
    }
    //界面上插入节点
    insertLNode(ui->comboBoxInsert->currentText().toInt(),edit);

    //调整右侧控件状态及值
    //下拉框等会相应变化
    adjustController();
    ui->lineEditState->setPalette(Qt::GlobalColor::green);
    ui->lineEditState->setText("Insert Success!");
}

//随机插入五个节点到链表末尾
void LinkList::on_pushButtonRandomInsert5_clicked()
{
    qDebug()<<"on_pushButtonRandomInsert5_clicked 随机插入五个节点到链表末尾"<<"\n";
    for(int i=0;i<5;++i)
    {
        setLinkListNormalBrush();
sleep(sleepTime);
        insertLNode(countNode+1,QString::number(rand()%999999999));
        adjustController();
        ui->lineEditState->setPalette(Qt::GlobalColor::green);
        ui->lineEditState->setText("Insert Success!");
    }
}

//槽函数：点击删除
void LinkList::on_pushButtonDelete_clicked()
{
    qDebug()<<"on_pushButtonDelete_clicked 槽函数：点击删除"<<"\n";
    setLinkListNormalBrush(); //灰色
sleep(sleepTime);
    QString deleteData;
    //调用deleteLNode
    deleteLNode(ui->comboBoxDelete->currentText().toInt(),deleteData);

    //调整右侧控件状态及值
    adjustController();
    ui->lineEditDelete->setText(deleteData);
    ui->lineEditState->setPalette(Qt::GlobalColor::green);
    ui->lineEditState->setText("Delete Success!");
}

//槽函数：点击查找
void LinkList::on_pushButtonLocate_clicked()
{
    qDebug()<<"on_pushButtonLocate_clicked 槽函数：点击查找"<<"\n";
    setLinkListNormalBrush();
sleep(sleepTime);
    QString edit=ui->lineEditLocate->text(); //接收输入的

    //若输入无效或未输入
    if(edit.isEmpty())
    {
        adjustController();
        ui->lineEditState->setPalette(Qt::GlobalColor::red);
        ui->lineEditState->setText("Please Input!");
        return;
    }

    //根据查找结果给出结果输出
    int pos;
    if(locateLNode(pos,edit))
    {
        ui->comboBoxLocate->addItem(QString::number(pos));
        ui->comboBoxLocate->setCurrentText(QString::number(pos));
        ui->lineEditState->setPalette(Qt::GlobalColor::green);
        ui->lineEditState->setText("Locate Success!");
    }
    else
    {
        adjustController();
        ui->lineEditState->setPalette(Qt::GlobalColor::red);
        ui->lineEditState->setText("Locate Fail!");
    }
}

//调整演示的速度快慢
void LinkList::on_horizontalSlider_valueChanged(int value)
{
    qDebug()<<"on_horizontalSlider_valueChanged 调整演示的速度快慢"<<"\n";
//    qDebug()<<value;
    //value应该是slider自带的值的大小
    sleepTime=MAX_SLEEP_TIME/(value+1);

}
